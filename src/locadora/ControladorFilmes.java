/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Tiago
 */
public class ControladorFilmes {

    private ArrayList<Filme> filmes = new ArrayList<>();
    private Filme f;
    
    void addFilme(Filme f) {
        filmes.add(f);
    }
    
    public void cadastrarFilme(){
        int cod;
        cod = Integer.parseInt(JOptionPane.showInputDialog(null, "Insira o codigo do filme"));
        String nome = JOptionPane.showInputDialog(null, "Insira o nome do filme");
        double valor = Double.parseDouble(JOptionPane.showInputDialog(null, "Insira o valor da diaria"));
        f = new Filme(nome, cod, valor);
        addFilme(f);
    }

    public String listarFilmes() {
        String mensagem = "COD    -             NOME           -            STATUS \n";
        int i = 1;

        for (Filme filme : filmes) {
            mensagem = mensagem +filme.getId()+ "   -    " + filme.getNome() + "     -     " + filme.getStatus()
                    + "\n__________________________________________________________________\n";
            i++;
        }

        return mensagem;
    }
    
    public void trocaStatus(int cod){
        int i=0;
        this.pegaFilmePeloCod(cod).setStatus("Alugado");
        JOptionPane.showMessageDialog(null, "Filme Alugado com Sucesso!");
    }

    public int posFilme(int cod){
        int i=0;
        for (; i<this.filmes.size();i++) {
        if(this.filmes.get(i).getId()==cod) break;
        }
        return i;
        
    }
    
    public void mostrarFilmesDisponiveis() {
        String mensagem = "";
        int i = 1;

        for (Filme filme : filmes) {
            if (filme.getStatus().equals("disponível")) {
                mensagem = mensagem + i + "-Nome: " + filme.getNome() + "\nStatus: " + filme.getStatus()
                        + "\n-----------------------------------------\n";
                i++;
            }
        }

        JOptionPane.showInternalMessageDialog(null, mensagem);
    }
    
    public boolean temDisponiblidadePara(int cod){
        for (Filme filme : filmes) {
            if(filme.getId()==cod){
                return filme.getStatus().equals("disponível");   
            }

        }
        return false;
    }
        
    public Filme pegaFilmePeloCod(int cod){
        for (Filme filme : filmes) {
            if (filme.getId()==cod) {
                return filme;
            }
        }
        return null;
    }
}

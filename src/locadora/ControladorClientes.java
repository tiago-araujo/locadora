/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Tiago
 */
public class ControladorClientes {

    private ArrayList<Cliente> clientes = new ArrayList<>();
    private Cliente c;
    
    
    void addClient(Cliente c) {
        boolean d = true;
        String m = "Usuário adicionado com sucesso!";
        if(!this.clientes.isEmpty()){
            for (Cliente cliente : clientes) {
                if(c.getRG().equals(cliente.getRG())){
                  d = false;
                  m = "Usuário existente!";
                }
            }
        }
        if(d){
            this.clientes.add(c);
            JOptionPane.showMessageDialog(null, m);
        }
        
    }

    public void cadastrarCliente(){
        String nome = JOptionPane.showInputDialog(null, "Digite seu nome");
        String rg = JOptionPane.showInputDialog(null, "Digite o numero do seu Doc:");
        c = new Cliente(nome, rg);
        addClient(c);
    }
    
    
//    public void exibeClientes() {
//        String mensagem = "";
//        if (this.clientes.isEmpty()) {
//            mensagem = "Não existe Clientes Cadastrados!";
//        } else {
//            for (Cliente cliente : clientes) {
//                mensagem = mensagem + "Nome: " + cliente.getNome() + "\nNumero do doc: " + cliente.getRG()
//                        + "\n--------------------------------\n";
//            }
//        }
//
//        JOptionPane.showInputDialog(null, mensagem);
//    }
    
    public boolean temCadastroDe(String doc){
        boolean d = false;
        if(!this.clientes.isEmpty()){
            for (Cliente cliente : clientes) {
                if(cliente.getRG().equals(doc) ){
                  d = true;
                }
            }
        }
        return d;
    }
    
    public boolean estaVazia(){
        return this.clientes.isEmpty();
    }
    
    public Cliente pegaClientePeloDoc(String doc){
        for (Cliente cliente : clientes) {
            if (cliente.getRG().equals(doc)) {
                return cliente;
            }
        }
        return null;
    }
}

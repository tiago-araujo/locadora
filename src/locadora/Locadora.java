/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

import javax.swing.JOptionPane;

/**
 *
 * @author Tiago
 */
public class Locadora {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        int e = JOptionPane.showConfirmDialog(null, "OLA! Bom dia, Bem vindo ao sistema de locação de Filmes.\nAntes de abrir a locadora devemos registrar no sistema todos filme de nossa locadora\nDeseja fazer isso manualmente?\n\nEscolhendo SIM será habilitado o modo de cadastro manual.\nSe escolher NÃO nosso sistema fará isso automaticamente para você.");

        ControladorClientes listaDeClientes = new ControladorClientes();
        ControladorFilmes listaDeFilmes = new ControladorFilmes();
        ControladorDeContas listaDeContas = new ControladorDeContas();

        if (e == 1) {
            System.out.println("sistema de cadastro automatico de filmes");
            Filme f1 = new Filme("A volta dos que não foram", 11, 2.5);
            Filme f2 = new Filme("O pente do rei careca", 12, 2.5);
            Filme f3 = new Filme("O naufragio no deserto", 13, 2.5);
            Filme f4 = new Filme("Ta chovendo canivete 2", 15, 2.5);
            Filme f5 = new Filme("Em busca da cola", 33, 2.5);

            listaDeFilmes.addFilme(f1);
            listaDeFilmes.addFilme(f2);
            listaDeFilmes.addFilme(f3);
            listaDeFilmes.addFilme(f4);
            listaDeFilmes.addFilme(f5);
        } else if(e == 0){

            for (;;) {
                listaDeFilmes.cadastrarFilme();
                if (JOptionPane.showConfirmDialog(null, "Você deseja cadastrar outro filme?") != 0) {
                    break;
                }
            }

        }else{
            System.exit(0);
        }

        Cliente cliente;
        Conta conta;
        String rg;
        String msg = "";

        for (;;) {
            String op = JOptionPane.showInputDialog(null, "Ola, Bem Vindo ao Sistema de Locação de Filmes. Por favor escolhar uma das opções abaixo:\n1-Sou Cliente Cadastrado;\n2-Não possuo Cadastro;\n3-Quero apenas ver a lista de Filmes;\n4-Sair");
            switch (op) {
                case "1":
                    if (listaDeClientes.estaVazia()) {
                        JOptionPane.showMessageDialog(null, "Por favor faça seu cadastro primeiro");
                    } else {
                        rg = JOptionPane.showInputDialog(null, "Digite o numero do seu Doc:");
                        if (listaDeClientes.temCadastroDe(rg)) {
                            cliente = listaDeClientes.pegaClientePeloDoc(rg);
                            listaDeContas.addConta(cliente);
                            conta = listaDeContas.pegaContaPeloCliente(cliente);
                            for (;;) {
                                int filmeEscolhido = Integer.parseInt(JOptionPane.showInputDialog(null, "Por favor digite o codigo do filme que você deseja alocar:\n" + listaDeFilmes.listarFilmes()));
                                System.out.println("cod>" + filmeEscolhido);
                                if (listaDeFilmes.temDisponiblidadePara(filmeEscolhido)) {
                                    int dias = Integer.parseInt(JOptionPane.showInputDialog(null, "Quantos dias você vai ficar com o filme?"));
                                    conta.addGasto(listaDeFilmes.pegaFilmePeloCod(filmeEscolhido), dias);
                                    listaDeFilmes.trocaStatus(filmeEscolhido);
                                } else {
                                    JOptionPane.showMessageDialog(null, "Filme indisponivel");
                                }

                                int op2 = JOptionPane.showConfirmDialog(null, "Você deseja escolher outro filme?");
                                if (op2 != 0) {
                                    break;
                                }
                            }
                            while (conta.getDebto() != 0.0) {
                                JOptionPane.showMessageDialog(null, conta.listaGastos());
                                String formaDepagamento = JOptionPane.showInputDialog(null, "Total a pagar: " + conta.getDebto() + "\nEscolha a forma de pagamento:\n 1- Dinheiro\n 2- Cartão de Credito");

                                switch (formaDepagamento) {
                                    case "1":
                                        listaDeContas.pagarADinheiroAConta(conta);
                                        break;

                                    case "2":
                                        listaDeContas.pagarComCartaoAConta(conta);
                                        break;

                                    default:
                                        JOptionPane.showMessageDialog(null, "opção invalida");
                                }
                            }

                        }
                    }
                    break;

                case "2":
                    listaDeClientes.cadastrarCliente();
                    break;

                case "3":
                    JOptionPane.showMessageDialog(null, listaDeFilmes.listarFilmes());
                    break;

                case "4":
                    JOptionPane.showMessageDialog(null, "Obrigado por usar nosso sistema");
                    System.exit(1);
                    break;

                default:

                    break;
            }
        }
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Tiago
 */
public class ControladorDeContas {
    private ArrayList<Conta> contas = new ArrayList<>();
    private Conta conta;
    
    public void addConta(Cliente c){    
        if(!this.temContaParaCliente(c)){
            this.conta = new Conta(c);
            this.contas.add(conta);
            System.out.println("Conta criada!");
        }else{
            System.out.println("ja existe conta para esse Cliente");
        }
    }
    
    
    public String mostraContas(){
        String mensagem = "";

        for (Conta conta : contas) {
            mensagem = mensagem +conta.listaGastos()+
                    "\n_____________________________________________\n";
        }
        return mensagem;
    }
    
    public Conta pegaContaPeloCliente(Cliente c){
        for (Conta conta1 : contas) {
            if(conta1.getC().getRG().equals(c.getRG())){
                return conta1;
            }
        }
        return null;
    }
    
    public boolean temContaParaCliente(Cliente c){
        for (Conta conta1 : contas) {
            if(conta1.getC().getRG().equals(c.getRG())){
                return true;
            }
        }
        return false;
    }
    
    public void pagarADinheiroAConta(Conta conta){
        String msg = "";
        double dinheiro = Double.parseDouble(JOptionPane.showInputDialog(null, "Total a pagar: "+conta.getDebto() +"\nInsira o dinheiro que o cliente deu:"));
        double troco = dinheiro-conta.getDebto();
        
        if(troco>=0){
            msg = "Total a pagar: "+conta.getDebto()+"\nDinheiro:  R$"+dinheiro+"\nTroco:  R$"+troco;
        }else{
            msg = "Total a pagar: "+conta.getDebto()+"\nDinheiro:  R$"+dinheiro+"\nTroco:  R$0.00";
        }
        conta.alteraDebto(dinheiro);
        JOptionPane.showMessageDialog(null, msg);
        
    }
    
    public void pagarComCartaoAConta(Conta conta){
        double total = conta.getDebto() + conta.getDebto()*0.005;
        JOptionPane.showMessageDialog(null, "Total a pagar: "+total+"\nDinheiro:  R$"+total+"\nTroco:  R$0.00");
        conta.alteraDebto(total);
    }
}

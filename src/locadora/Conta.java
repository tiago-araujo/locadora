/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locadora;

import java.util.ArrayList;

/**
 *
 * @author Tiago
 */
public class Conta {
    private Cliente c;
    private ArrayList<Filme> filmes = new ArrayList<>();
    private double debto;

    public Conta(Cliente c) {
        this.c = c;
        this.debto = 0;
    }
    
    public void addGasto(Filme f, int dias){
        filmes.add(f);
        this.debto += f.getDiaria()*dias;
    }
        
    public String listaGastos(){
        String mensagem = "Cliente: "+this.c.getNome();
        double gastoTotal = 0;
        for (Filme filme : filmes) {
            mensagem += "\n+"+filme.getNome()+" - R$"+filme.getDiaria()+"/dia";
            
        }
        mensagem += "\nTotal = "+ this.debto;
        return mensagem;
    }

    public Cliente getC() {
        return c;
    }

    public ArrayList<Filme> getFilmes() {
        return filmes;
    }

    public double getDebto() {
        return debto;
    }
    public void alteraDebto(double x){
        this.debto -= x;
        if(this.debto<0) this.debto=0;
    }
    
    
    
}
